Book Store
==========

Pour démarrer l'application : `./gradlew run`

----

*BookStore* est une application développée dans le cadre d'un entretien technique.
Son objectif est de déterminer pour une liste de livres, quel est le cumul de réductions le plus intéressant pour un client.

__Note__ : Cette version a l'inconvénient d'être lente à la détermination du meilleur cumul de réduction. Le tableau ci-dessous détail les temps de calculs moyens par nombre d'articles :

| Nb Articles | Temps |
|-------------|-------|
| 7 et moins  | 0s    |
| 8           | < 1s  |
| 9           | > 2s  |
| 10          | > 4s  |
| 11          | > 10s |

package com.mpapillon.bookstore;

import com.mpapillon.bookstore.exceptions.QuantityOutOfRangeException;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class DefaultShoppingCartTest {

    public static final String NOMBRE_ARTICLES = "Le panier n'a pas le bon nombre d'articles";
    public static final String ARTICLE = "Le panier ne possède pas le bon article";
    public static final String MONTANT = "Le panier n'a pas le bon montant";

    @Test
    public void add() {
        IShoppingCart cart = new DefaultShoppingCart();
        cart.add(CommonIProducts.BOOK_1);
        cart.add(CommonIProducts.BOOK_2);

        assertEquals(NOMBRE_ARTICLES, 2, cart.size());
        assertEquals("Le nombre d'articles unique n'est n'est pas bon", 2, cart.getProductsSet().size());
        assertTrue(ARTICLE, cart.getProducts().contains(CommonIProducts.BOOK_1));
        assertTrue(ARTICLE, cart.getProducts().contains(CommonIProducts.BOOK_2));
        assertEquals(MONTANT, new BigDecimal(16), cart.getTotalAmount());
    }

    @Test
    public void add_WithQuantity() {
        IShoppingCart cart = new DefaultShoppingCart();
        cart.add(CommonIProducts.BOOK_1, 2);
        cart.add(CommonIProducts.BOOK_2);

        assertEquals(NOMBRE_ARTICLES, 3, cart.size());
        assertEquals("La quantité de l'article n'est pas bonne", 2, cart.getQuantity(CommonIProducts.BOOK_1));
        assertEquals("Le nombre d'articles unique n'est n'est pas bon", 2, cart.getProductsSet().size());
        assertTrue(ARTICLE, cart.getProducts().contains(CommonIProducts.BOOK_1));
        assertTrue(ARTICLE, cart.getProducts().contains(CommonIProducts.BOOK_2));
        assertEquals(MONTANT, new BigDecimal(24), cart.getTotalAmount());
    }

    @Test(expected = QuantityOutOfRangeException.class)
    public void add_TooManyProducts() {
        IShoppingCart cart = new DefaultShoppingCart();
        cart.add(CommonIProducts.BOOK_2);
        cart.add(CommonIProducts.BOOK_2, DefaultShoppingCart.MAX_SIZE);
    }

    @Test(expected = QuantityOutOfRangeException.class)
    public void add_TooManyQuantity() {
        IShoppingCart cart = new DefaultShoppingCart();
        cart.add(CommonIProducts.BOOK_2, DefaultShoppingCart.MAX_SIZE + 5);
    }

}
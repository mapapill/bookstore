package com.mpapillon.bookstore;

import java.math.BigDecimal;

public interface CommonIProducts {
    IProduct BOOK_1 = new Book(1L, "Test 1", new BigDecimal(8));
    IProduct BOOK_2 = new Book(2L, "Test 2", new BigDecimal(8));
    IProduct BOOK_3 = new Book(3L, "Test 3", new BigDecimal(8));
    IProduct BOOK_4 = new Book(4L, "Test 4", new BigDecimal(8));
    IProduct BOOK_5 = new Book(5L, "Test 5", new BigDecimal(8));
}

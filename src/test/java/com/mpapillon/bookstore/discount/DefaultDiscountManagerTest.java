package com.mpapillon.bookstore.discount;

import com.mpapillon.bookstore.CommonIProducts;
import com.mpapillon.bookstore.DefaultShoppingCart;
import com.mpapillon.bookstore.IShoppingCart;
import com.mpapillon.bookstore.discount.strategies.*;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class DefaultDiscountManagerTest {
    private static final String RÉDUCTIONS = "Mauvaises réductions";
    private static final String MONTANT_DES_RÉDUCTIONS = "Erreur dans le montant des réductions";

    private Set<IDiscountStrategy> discountStrategies;
    private IShoppingCart cart;

    @Before
    public void setUp() throws Exception {
        discountStrategies = new HashSet<>(4);

        discountStrategies.add(new TwoBooksDiscountStrategy());
        discountStrategies.add(new ThreeBooksDiscountStrategy());
        discountStrategies.add(new FourBooksDiscountStrategy());
        discountStrategies.add(new FiveBooksDiscountStrategy());

        this.cart = new DefaultShoppingCart();
        cart.add(CommonIProducts.BOOK_1);
        cart.add(CommonIProducts.BOOK_2);
    }

    @Test
    public void apply() {
        IDiscountManager manager = new DefaultDiscountManager(discountStrategies);
        DiscountAmount amount = manager.apply(this.cart);

        assertEquals(RÉDUCTIONS, 1, amount.getDiscounts().size());
        assertEquals(MONTANT_DES_RÉDUCTIONS, new BigDecimal(15.20).setScale(2, RoundingMode.HALF_UP), amount.getAmount());

        cart.add(CommonIProducts.BOOK_1);
        amount = manager.apply(this.cart);

        assertEquals(RÉDUCTIONS, 2, amount.getDiscounts().size());
        assertEquals(MONTANT_DES_RÉDUCTIONS, new BigDecimal(23.20).setScale(2, RoundingMode.HALF_UP), amount.getAmount());

        cart.add(CommonIProducts.BOOK_2);
        cart.add(CommonIProducts.BOOK_3, 2);
        cart.add(CommonIProducts.BOOK_4);
        cart.add(CommonIProducts.BOOK_5);

        amount = manager.apply(this.cart);

        assertEquals(RÉDUCTIONS, 2, amount.getDiscounts().size());
        assertEquals(MONTANT_DES_RÉDUCTIONS, new BigDecimal(51.20).setScale(2, RoundingMode.HALF_UP), amount.getAmount());
    }
}
package com.mpapillon.bookstore;

import com.mpapillon.bookstore.discount.DefaultDiscountManager;
import com.mpapillon.bookstore.discount.Discount;
import com.mpapillon.bookstore.discount.DiscountAmount;
import com.mpapillon.bookstore.discount.IDiscountManager;
import com.mpapillon.bookstore.discount.strategies.*;

import java.math.BigDecimal;
import java.util.*;

import static com.mpapillon.bookstore.utils.CommonUtils.executionTimeOf;

/**
 * <p><i>BookStore</i> est une application d&eacute;velopp&eacute;e dans le cadre d'un entretien technique.
 * <p>Son objectif est de d&eacute;terminer pour une liste de livres, quel est le cumul de r&eacute;ductions
 * le plus int&eacute;ressant pour un client.
 * <p><b>Note</b>: Cette version a l'inconv&eacute;nient d'&ecirc;tre lente &agrave; la d&eacute;termination du meilleur
 * cumul de r&eacute;duction. Le tableau ci-dessous d&eacute;tail les temps de calculs moyens par nombre d'articles :
 * <table summary="" border="1">
 * <tr>
 * <th>Articles</th> <th>Temps</th>
 * </tr>
 * <tr>
 * <td>7 et moins</td> <td>0s</td>
 * </tr>
 * <tr>
 * <td>8</td> <td>&lt; 1s</td>
 * </tr>
 * <tr>
 * <td>9</td> <td>&gt; 2s</td>
 * </tr>
 * <tr>
 * <td>10</td> <td>&gt; 4s</td>
 * </tr>
 * <tr>
 * <td>11</td> <td>&gt; 19s</td>
 * </tr>
 * </table>
 */
public final class BookStore {
    public static void main(String[] args) {
        final List<Book> books = buildBooks();
        final IDiscountManager discountManager = buildDiscountManager();
        final IShoppingCart shoppingCart = buildShoppingCart(books);

        System.out.printf("Nombre de produits dans le panier : %s\n", shoppingCart.size());
        System.out.printf("Prix initial : %s€ \n", shoppingCart.getTotalAmount());

        long elapsedTime = executionTimeOf(() -> {
            final DiscountAmount discount = discountManager.apply(shoppingCart);
            System.out.println("Remises appliquées :");
            discount.getDiscounts().stream()
                    .map(Discount::getStrategy)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(d -> System.out.printf("  * %s\n", d.getName()));
            System.out.printf("Prix avec remises : %s€ \n", discount.getAmount());
        });

        System.out.printf("[Durée du traitement : %ss]\n", elapsedTime / 1000d);
    }

    /**
     * @return Construit une collection de livres.
     */
    private static List<Book> buildBooks() {
        final List<Book> books = new ArrayList<>(5);

        books.add(new Book(1L, "Tome I", new BigDecimal(8)));
        books.add(new Book(2L, "Tome II", new BigDecimal(8)));
        books.add(new Book(3L, "Tome III", new BigDecimal(8)));
        books.add(new Book(4L, "Tome IV", new BigDecimal(8)));
        books.add(new Book(5L, "Tome V", new BigDecimal(8)));

        return books;
    }

    /**
     * @return Construit un gestionnaire de remises avec des strat&eacute;gies.
     */
    private static IDiscountManager buildDiscountManager() {
        final Set<IDiscountStrategy> discountStrategies = new HashSet<>(4);

        discountStrategies.add(new TwoBooksDiscountStrategy());
        discountStrategies.add(new ThreeBooksDiscountStrategy());
        discountStrategies.add(new FourBooksDiscountStrategy());
        discountStrategies.add(new FiveBooksDiscountStrategy());

        return new DefaultDiscountManager(discountStrategies);
    }

    /**
     * Construit un panier &agrave; partir d'une collection de livres.
     *
     * @param books Collection de livres.
     * @return Panier.
     */
    private static IShoppingCart buildShoppingCart(List<Book> books) {
        final IShoppingCart shoppingCart = new DefaultShoppingCart();
        shoppingCart.add(books.get(0), 2);
        shoppingCart.add(books.get(1), 2);
        shoppingCart.add(books.get(2), 2);
        shoppingCart.add(books.get(3));
        shoppingCart.add(books.get(4));

        return shoppingCart;
    }
}

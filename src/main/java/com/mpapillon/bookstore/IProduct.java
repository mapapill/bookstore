package com.mpapillon.bookstore;

import java.math.BigDecimal;

public interface IProduct {
    long getId();

    BigDecimal getPrice();
}

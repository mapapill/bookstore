package com.mpapillon.bookstore.utils;

public interface CommonUtils {
    /**
     * Calcule le temps d'ex&eacute;cution d'un {@link Runnable}.
     *
     * @param runnable Runnable &agrave; mesurer.
     * @return temps d'ex&eacute;cution en millisecondes.
     */
    static long executionTimeOf(Runnable runnable) {
        long startTime = System.currentTimeMillis();
        runnable.run();
        long stopTime = System.currentTimeMillis();

        return stopTime - startTime;
    }
}

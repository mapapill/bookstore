package com.mpapillon.bookstore.utils;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public final class StreamUtils {
    private StreamUtils() {
    }

    /**
     * <p>Obtient un {@link Stream} contenant toutes les combinaisons d'une {@link Collection}.<br>
     * <code>[A, B, C] -&gt; [[A], [B], [A, B], [C], [A, C], [B, C], [A, B, C]]</code>
     *
     * <p> Depuis : <a href="https://stackoverflow.com/a/28515871/3311411">StackOverflow</a>.
     *
     * @param collection Collection &agrave; utiliser.
     * @param <T>        Type des &eacute;l&eacute;ments de la collection.
     * @return Un flux avec les combinaisons possibles.
     */
    public static <T> Stream<List<T>> combinations(Collection<T> collection) {
        // there are 2 ^ list.size() possible combinations
        // stream through them and map the number of the combination to the combination
        List<T> list = new ArrayList<>(collection);
        return LongStream.range(1, 1 << list.size())
                .mapToObj(l -> bitMapToList(list, l));
    }

    private static <T> List<T> bitMapToList(List<T> list, long... bitmap) {
        return BitSet.valueOf(bitmap)
                // Retourne un flux avec les indexes des bits valorisés.
                // Exemple :
                //    Idx: 012
                //     5 = 101b = [0, 2]
                .stream()
                .mapToObj(list::get)
                .collect(Collectors.toList());
    }
}

package com.mpapillon.bookstore;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * Cette interface d&eacute;fini un "panier" d'achats.
 * Il s'agit d'une liste d'articles ({@link IProduct}) et leur quantit&eacute;.
 */
public interface IShoppingCart {
    /**
     * Ajoute un article au panier ou incr&eacute;mente la quantit&eacute; du pr&eacute;sent article de 1.
     *
     * @param product Article &agrave; ajouter ou incr&eacute;menter.
     */
    void add(IProduct product);

    /**
     * Ajoute un article au panier et sa quantit&eacute; souhait&eacute;e
     * ou incr&eacute;mente la quantit&eacute; du pr&eacute;sent article.
     *
     * @param product Article &agrave; ajouter ou incr&eacute;menter.
     * @param quantity Quantit&eacute; d'article.
     */
    void add(IProduct product, int quantity);

    /**
     * @return Obtient une liste de tous les produits pr&eacute;sents dans le panier.
     */
    List<IProduct> getProducts();

    /**
     * @return Obtient une liste de tous les produits uniques pr&eacute;sents dans le panier.
     */
    Set<IProduct> getProductsSet();

    /**
     * Obtient la quantit&eacute; d'un {@link IProduct} du panier.
     *
     * @param product Produit &agrave; compter.
     * @return La quantit&eacute; du produit ou 0 s'il n'est pas dans le panier.
     */
    int getQuantity(IProduct product);

    /**
     * @return Obtient le nombre total d'articles dans le panier.
     */
    int size();

    /**
     * @return Obtient le prix total du panier sans remises.
     */
    BigDecimal getTotalAmount();
}

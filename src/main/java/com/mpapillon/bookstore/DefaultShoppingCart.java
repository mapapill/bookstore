package com.mpapillon.bookstore;

import com.mpapillon.bookstore.exceptions.QuantityOutOfRangeException;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Impl&eacute;mentation par d&eacute;faut de l'interface {@link IShoppingCart}.
 * Ce panier est limit&eacute; &agrave; 10 articles.
 */
public class DefaultShoppingCart implements IShoppingCart {
    /**
     * Nombre maximal d'articles du panier.
     */
    public static final int MAX_SIZE = 10;

    private Map<IProduct, Integer> cart = new LinkedHashMap<>();

    private Stream<IProduct> getProductsStream() {
        return this.cart.entrySet()
                .stream()
                .flatMap(e -> Collections.nCopies(e.getValue(), e.getKey()).stream());
    }

    @Override
    public void add(IProduct product) throws QuantityOutOfRangeException {
        this.add(product, 1);
    }

    @Override
    public void add(IProduct product, int quantity) throws QuantityOutOfRangeException {
        if (quantity <= 0 || quantity > MAX_SIZE)
            throw new QuantityOutOfRangeException(String.format("La quantité doit être comprise entre 1 et %s.", MAX_SIZE));
        if (this.size() + quantity > MAX_SIZE)
            throw new QuantityOutOfRangeException(String.format("Le panier ne peut pas contenir plus de %s articles.", MAX_SIZE));
        int actualQte = this.cart.getOrDefault(product, 0);
        this.cart.put(Objects.requireNonNull(product), actualQte + quantity);
    }

    @Override
    public List<IProduct> getProducts() {
        return getProductsStream().collect(Collectors.toList());
    }

    @Override
    public Set<IProduct> getProductsSet() {
        return this.cart.keySet();
    }

    @Override
    public int getQuantity(IProduct product) {
        return this.cart.getOrDefault(product, 0);
    }

    @Override
    public int size() {
        return (int) getProductsStream().count();
    }

    @Override
    public BigDecimal getTotalAmount() {
        return getProductsStream()
                .map(IProduct::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}

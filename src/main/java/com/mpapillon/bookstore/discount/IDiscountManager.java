package com.mpapillon.bookstore.discount;

import com.mpapillon.bookstore.IShoppingCart;
import com.mpapillon.bookstore.discount.strategies.IDiscountStrategy;

import java.util.Collection;

/**
 * Cette interface &eacute; un gestionnaire de remises, il a pour rôle de
 * regrouper des strat&eacute;gies de remises ({@link IDiscountStrategy}) et de d&eacute;duire
 * le montant d'un panier ({@link IShoppingCart}) en fonction des remises applicables.
 */
public interface IDiscountManager {
    /**
     * Ajoute une instance de {@link IDiscountStrategy} au gestionnaire de remises.
     *
     * @param strategy Strat&eacute;gie de remise &agrave; ajouter.
     */
    void addStrategy(IDiscountStrategy strategy);

    /**
     * Ajoute plusieurs instances de {@link IDiscountStrategy} au gestionnaire de remises.
     *
     * @param strategies Strat&eacute;gies de remise &agrave; ajouter.
     */
    void addAllStrategy(Collection<IDiscountStrategy> strategies);

    /**
     * Applique les remises &agrave; une instance de {@link IShoppingCart}.
     *
     * @param shoppingCart Panier &agrave; appliquer les remises.
     * @return Le montant du panier apr&egrave;s remises.
     */
    DiscountAmount apply(IShoppingCart shoppingCart);
}

package com.mpapillon.bookstore.discount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Cette classe en lecture seule repr&eacute;sente une r&eacute;duction sur un panier &agrave; partir d'une liste de {@link Discount}.
 * Elle offre la possibilit&eacute; d'obtenir le montant total apr&egrave;s application des remises.<br>
 * <p>
 * <b>Note</b>:
 * </p>
 */
public class DiscountAmount implements Comparable<DiscountAmount> {
    final private List<Discount> discounts;

    public DiscountAmount(Collection<Discount> c) {
        this.discounts = new ArrayList<>(c);
    }

    /**
     * @return Obtient le montant total apr&egrave;s application des remises.
     */
    public BigDecimal getAmount() {
        return this.discounts.stream()
                .map(Discount::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * @return Obtient les instances de {@link Discount}.
     */
    public Collection<Discount> getDiscounts() {
        return new ArrayList<>(this.discounts);
    }

    @Override
    public int compareTo(DiscountAmount o) {
        return this.getAmount().compareTo(o.getAmount());
    }
}

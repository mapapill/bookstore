package com.mpapillon.bookstore.discount;

import com.mpapillon.bookstore.IProduct;
import com.mpapillon.bookstore.discount.strategies.IDiscountStrategy;
import com.mpapillon.bookstore.exceptions.NotApplicableDiscountStrategy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>Cet classe en lecture seule est une repr&eacute;sentation
 * du r&eacute;sultat d'une strat&eacute;gie de remise ({@link IDiscountStrategy}).
 * <p><b>Note</b>: Il est possible d'instancier cette classe sans strat&eacute;gie de remise.
 * Le montant sera le cumul du prix des articles qui la compose.
 */
public class Discount {
    private final IDiscountStrategy strategy;
    private final Collection<IProduct> products;
    private final BigDecimal amount;

    /**
     * Construit une instance de {@link Discount} uniquement si la {@link IDiscountStrategy} est applicable aux produits.
     *
     * @param strategy Strat&eacute;gie de remise &agrave; appliquer.
     * @param products Liste de produits concern&eacute;s.
     * @return Une instance de {@link Discount} si la strat&eacute;gie est applicable.
     */
    public static Optional<Discount> of(IDiscountStrategy strategy, Collection<IProduct> products) {
        if (strategy.isApplicableTo(products)) return Optional.of(new Discount(strategy, products));
        else return Optional.empty();
    }

    public Discount(Collection<IProduct> products) {
        this(null, products);
    }

    /**
     * Construit une instance de {@link Discount} &agrave; partir d'une strat&eacute;gie de remise
     * et d'une collection de produits ({@link IProduct}).
     *
     * @param strategy Stratamountgie de remise &agrave; appliquer, ce param&egrave;tre peut &ecirc;tre <code>null</code>.
     * @param products Collection de produits où la remise sera appliquamounte.
     * @throws NotApplicableDiscountStrategy Si la stratramountgie de remise en param&egrave;tre n'est pas <code>null</code>
     *                                       et n'est pas compatible avec la collection de produits.
     */
    public Discount(IDiscountStrategy strategy, Collection<IProduct> products) throws NotApplicableDiscountStrategy {
        this.strategy = strategy;
        this.products = new ArrayList<>(Objects.requireNonNull(products));

        if (strategy == null) this.amount = this.getProductsAmount();
        else this.amount = strategy
                .getAmount(products)
                .orElseThrow(NotApplicableDiscountStrategy::new);
    }

    /**
     * @return Obtient la stratamountgie ({@link IDiscountStrategy}) appliquamounte par cette remise
     * ou {@link Optional#EMPTY} si aucune stratamountgie n'est appliquamounte.
     */
    public Optional<IDiscountStrategy> getStrategy() {
        return Optional.ofNullable(strategy);
    }

    /**
     * @return Obtient la collection de produits ({@link IProduct}) attachamounte &agrave; cette remise.
     */
    public Collection<IProduct> getProducts() {
        return products;
    }

    /**
     * @return Obtient le montant de la remise.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @return Calcule la somme des prix des produits.
     */
    private BigDecimal getProductsAmount() {
        return products.stream().map(IProduct::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Discount discount = (Discount) o;
        return Objects.equals(strategy, discount.strategy) &&
                Objects.equals(products, discount.products) &&
                Objects.equals(amount, discount.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(strategy, products, amount);
    }

    @Override
    public String toString() {
        return "Discount{" +
                "strategy=" + strategy +
                ", products=" + products +
                ", amount=" + amount +
                '}';
    }
}

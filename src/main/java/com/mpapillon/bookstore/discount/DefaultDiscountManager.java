package com.mpapillon.bookstore.discount;

import com.mpapillon.bookstore.IProduct;
import com.mpapillon.bookstore.IShoppingCart;
import com.mpapillon.bookstore.discount.strategies.IDiscountStrategy;
import com.mpapillon.bookstore.utils.StreamUtils;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Cette classe est le gestionnaire de remise par d&eacute;faut, il s'agit d'une
 * impl&eacute;mentation de {@link IDiscountManager}. Ce gestionnaire recherche
 * le cumul de remises qui permettent d'avoir le montant le moins &eacute;lev&eacute; d'un panier.
 */
public class DefaultDiscountManager implements IDiscountManager {
    private final Set<IDiscountStrategy> strategies;
    private final ExecutorService executor = Executors.newWorkStealingPool();

    public DefaultDiscountManager() {
        this.strategies = new LinkedHashSet<>();
    }

    public DefaultDiscountManager(Collection<IDiscountStrategy> strategies) {
        this.strategies = new LinkedHashSet<>(strategies);
    }

    @Override
    public void addStrategy(IDiscountStrategy strategy) {
        this.strategies.add(strategy);
    }

    @Override
    public void addAllStrategy(Collection<IDiscountStrategy> strategies) {
        this.strategies.addAll(strategies);
    }

    /**
     * <p>Recherche et applique le meilleur cumul de remises &agrave; une instance de {@link IShoppingCart}.
     * <p><b>Attention</b> : Le calcul de la meilleure remise peut &ecirc;tre lent
     * si le panier poss&egrave;de 10 articles ou plus.</p>
     *
     * @param shoppingCart Panier &agrave; appliquer les remises.
     * @return Le montant du panier apr&egrave;s remises.
     */
    @Override
    public DiscountAmount apply(IShoppingCart shoppingCart) {
        final List<Discount> discounts = StreamUtils
                // Recupère toutes les associations possibles de produits ([A, B, C] -> [[A], [AB], [AC], [ABC], ...])
                .combinations(shoppingCart.getProductsSet())
                // Recupère les potentielles remises applicable aux différentes assoiations
                .flatMap(x -> this.strategies.stream().map(s -> Discount.of(s, x)))
                // Supprime les remises non applicables
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        return findBestDiscountAmount(true, shoppingCart.getProducts(), discounts);
    }

    /**
     * Recherche la meilleure association de remises pour une liste d'articles.
     *
     * @param runAsync  Si valeure &agrave; <code>true</code>, la fonction est execut&eacute;e dans des threads.
     * @param cart      Liste d'articles &agrave; analyser.
     * @param discounts Liste de remises &agrave; appliquer.
     * @return La meilleur association de remises.
     */
    private DiscountAmount findBestDiscountAmount(boolean runAsync, List<IProduct> cart, List<Discount> discounts) {
        // Il n'y a pas d'articles dans le "panier". Donc montant à 0 = liste vide.
        if (cart.isEmpty()) return new DiscountAmount(Collections.emptyList());

        // Contient les différentes associations de remises compatibles.
        final List<Callable<DiscountAmount>> callables = new ArrayList<>(discounts.size());

        discounts.forEach(discount -> {
            // Si la réduction n'est pas applicable sur le "panier" actuel, passe à la suivante.
            if (discount.getProducts().size() > cart.size()
                    || Collections.disjoint(cart, discount.getProducts())) return;
            callables.add(() -> {
                // Création d'un nouveau "panier" sans les produits remisés.
                final List<IProduct> newCart = new ArrayList<>(cart);
                discount.getProducts().forEach(newCart::remove);
                // Cumul de remises possibles.
                final List<Discount> discountPrices = new ArrayList<>();
                discountPrices.add(discount);
                // L'appel recursif ne doit pas être asynchrone pour préserver la bonne exécution du programme.
                discountPrices.addAll(findBestDiscountAmount(false, newCart, discounts).getDiscounts());

                return new DiscountAmount(discountPrices);
            });
        });

        final Collection<DiscountAmount> compatiblesDiscounts;

        if (runAsync) {
            try {
                compatiblesDiscounts = this.executor.invokeAll(callables)
                        .stream()
                        .map(future -> {
                            try {
                                return future.get();
                            } catch (InterruptedException | ExecutionException e) {
                                throw new IllegalStateException(e);
                            }
                        })
                        .collect(Collectors.toList());
            } catch (InterruptedException e) {
                throw new IllegalStateException();
            }
        } else {
            compatiblesDiscounts = callables
                    .stream()
                    .map(callable -> {
                        try {
                            return callable.call();
                        } catch (Exception e) {
                            throw new IllegalStateException(e);
                        }
                    })
                    .collect(Collectors.toList());
        }

        // Il n'y a pas de remises applicables, retourne le montant total des articles du "panier".
        if (compatiblesDiscounts.isEmpty()) {
            return new DiscountAmount(Collections.singletonList(new Discount(cart)));
        }

        // Retourne l'association de remise la moins chère.
        return compatiblesDiscounts.stream().min(DiscountAmount::compareTo).get();
    }

}

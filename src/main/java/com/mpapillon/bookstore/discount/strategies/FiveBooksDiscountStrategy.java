package com.mpapillon.bookstore.discount.strategies;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Remise de 25% pour l'achat de 5 livres diff&eacute;rents.
 */
public class FiveBooksDiscountStrategy extends AbstractBooksPercentageDiscountStrategy {
    private final BigDecimal discountValue = new BigDecimal(-0.25);

    @Override
    public String getName() {
        return "5 Livres - 25%";
    }

    @Override
    public BigDecimal getDiscountValue() {
        return this.discountValue;
    }

    @Override
    public int getNumberOfDifferentBooks() {
        return 5;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getClass().getName());
    }
}

package com.mpapillon.bookstore.discount.strategies;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Remise de 20% pour l'achat de 4 livres diff&eacute;rents.
 */
public class FourBooksDiscountStrategy extends AbstractBooksPercentageDiscountStrategy {
    private final BigDecimal discountValue = new BigDecimal(-0.20);

    @Override
    public String getName() {
        return "4 Livres - 20%";
    }

    @Override
    public BigDecimal getDiscountValue() {
        return this.discountValue;
    }

    @Override
    public int getNumberOfDifferentBooks() {
        return 4;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getClass().getName());
    }
}

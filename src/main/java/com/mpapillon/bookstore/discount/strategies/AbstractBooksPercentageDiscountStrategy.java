package com.mpapillon.bookstore.discount.strategies;

import com.mpapillon.bookstore.IProduct;
import com.mpapillon.bookstore.discount.DiscountType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

/**
 * Cette classe offre une impl&eacute;mentation de l'interface {@link IDiscountStrategy} affin
 * d'offrire une base pour les remises de type {@link DiscountType#PERCENTAGE}.
 */
public abstract class AbstractBooksPercentageDiscountStrategy implements IDiscountStrategy {
    public abstract int getNumberOfDifferentBooks();

    @Override
    public boolean isApplicableTo(Collection<IProduct> products) {
        return !products.isEmpty()
                && products.size() == this.getNumberOfDifferentBooks()
                // Les produits doivent être uniques
                && products.size() == new HashSet<>(products).size();
    }

    @Override
    public Optional<BigDecimal> getAmount(Collection<IProduct> products) {
        if (!isApplicableTo(products)) return Optional.empty();
        final BigDecimal sum = products.stream()
                .map(IProduct::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return Optional.of(sum.multiply(this.getDiscountValue()).add(sum).setScale(2, RoundingMode.HALF_UP));
    }

    @Override
    public DiscountType getType() {
        return DiscountType.PERCENTAGE;
    }

    @Override
    public abstract int hashCode();
}

package com.mpapillon.bookstore.discount.strategies;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Remise de 5% pour l'achat de 2 livres diff&eacute;rents.
 */
public class TwoBooksDiscountStrategy extends AbstractBooksPercentageDiscountStrategy {
    private final BigDecimal discountValue = new BigDecimal(-0.05);

    @Override
    public String getName() {
        return "2 Livres - 5%";
    }

    @Override
    public BigDecimal getDiscountValue() {
        return this.discountValue;
    }

    @Override
    public int getNumberOfDifferentBooks() {
        return 2;
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(this.getClass().getName());
    }
}

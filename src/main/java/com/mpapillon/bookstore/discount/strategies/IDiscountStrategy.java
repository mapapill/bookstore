package com.mpapillon.bookstore.discount.strategies;

import com.mpapillon.bookstore.IProduct;
import com.mpapillon.bookstore.discount.DiscountType;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Optional;

/**
 * Une strat&eacute;gie de remise s'applique sur une ou plusieurs instances de {@link IProduct},
 * elle peut &ecirc;tre calcul&eacute;e de mani&egrave;re fixe ({@link DiscountType#FIXED})
 * ou en pourcentage ({@link DiscountType#PERCENTAGE}).
 */
public interface IDiscountStrategy {
    /**
     * V&eacute;rifie si la remise est applicable sur la liste de {@link IProduct}.
     *
     * @param products Collection de produits &agrave; v&eacute;rifier.
     * @return <code>true</code> si la remise est applicable.
     */
    boolean isApplicableTo(Collection<IProduct> products);

    /**
     * Obtient la valeur de la remise. <br>
     * Si la remise est de type {@link DiscountType#PERCENTAGE} obtient le pourcentage de r&eacute;duction.
     *
     * @return Valeur de la remise.
     */
    BigDecimal getDiscountValue();

    /**
     * Calcul le prix de la remise sur une {@link Collection} de {@link IProduct}.
     *
     * @param products Collection de produits &agrave; utiliser pour la remise.
     * @return Le prix de la remise, ou {@link Optional#EMPTY} si la remise n'est pas applicable.
     */
    Optional<BigDecimal> getAmount(Collection<IProduct> products);

    /**
     * @return Obtient le nom commercial de la remise.
     */
    String getName();

    /**
     * @return Obtient le type de remise.
     */
    DiscountType getType();
}

package com.mpapillon.bookstore.discount.strategies;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Remise de 10% pour l'achat de 3 livres diff&eacute;rents.
 */
public class ThreeBooksDiscountStrategy extends AbstractBooksPercentageDiscountStrategy {
    private final BigDecimal discountValue = new BigDecimal(-0.10);

    @Override
    public String getName() {
        return "3 Livres - 10%";
    }

    @Override
    public BigDecimal getDiscountValue() {
        return this.discountValue;
    }

    @Override
    public int getNumberOfDifferentBooks() {
        return 3;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getClass().getName());
    }
}

package com.mpapillon.bookstore.discount;

/**
 * Cette &eacute;num&eacute;ration &eacute; les types de remises applicables.
 */
public enum DiscountType {
    PERCENTAGE, FIXED
}

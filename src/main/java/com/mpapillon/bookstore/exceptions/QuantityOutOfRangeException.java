package com.mpapillon.bookstore.exceptions;

import com.mpapillon.bookstore.IProduct;
import com.mpapillon.bookstore.IShoppingCart;

/**
 * Lev&eacute;e si le nombre d'articles d'un panier ({@link IShoppingCart})
 * ou la quantit&eacute; d'un article ({@link IProduct}) est hors des limites.
 */
public class QuantityOutOfRangeException extends RuntimeException {
    public QuantityOutOfRangeException(String message) {
        super(message);
    }
}

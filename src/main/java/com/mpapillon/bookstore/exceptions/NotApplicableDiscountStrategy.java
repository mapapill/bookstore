package com.mpapillon.bookstore.exceptions;

import com.mpapillon.bookstore.discount.strategies.IDiscountStrategy;

/**
 * Lev&eacute;e si une strat&eacute;gie de remise ({@link IDiscountStrategy})
 * non compatible est appliqu&eacute;e.
 */
public class NotApplicableDiscountStrategy extends RuntimeException {
}
